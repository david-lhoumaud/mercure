#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32 
#include <winsock2.h>
#include <windows.h>
#define close_socket closesocket // Windows utilise closesocket() au lieu de close() pour fermer les sockets
#else 
#include <unistd.h>
#include <arpa/inet.h>
#define close_socket close // Sur Linux, close() est utilisé pour fermer les sockets
#endif

#define BUFFER_SIZE 4096

int main(int argc, char *argv[]) {
    if (argc < 3) {
        printf("Usage: %s <server_ip> <server_port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    char *server_ip = argv[1];
    int server_port = atoi(argv[2]);

    int sock = 0;
    struct sockaddr_in serv_addr;
    char buffer[BUFFER_SIZE] = {0};

#ifdef _WIN32 
    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        printf("Failed. Error Code : %d", WSAGetLastError());
        exit(EXIT_FAILURE);
    }
#endif

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\033[31mSocket creation error\033[0m\n");
#ifdef _WIN32
        WSACleanup();
#endif
        return -1;
    }

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(server_port);

    if (inet_pton(AF_INET, server_ip, &serv_addr.sin_addr) <= 0) {
        printf("\033[31mInvalid address/ Address not supported\033[0m\n");
#ifdef _WIN32
        WSACleanup();
#endif
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\033[31mConnection Failed\033[0m\n");
#ifdef _WIN32
        WSACleanup();
#endif
        return -1;
    }
    
    printf("\033[32m☿ Start Mercure ☿\033[0m\n");

    while (1) {
        printf("\033[33m☿\033[0m ");
        
        fgets(buffer, BUFFER_SIZE, stdin);
        buffer[strcspn(buffer, "\n")] = 0;
        
        // Vérifier si la commande est vide
        if (strlen(buffer) == 0) {
            // Réafficher l'invite de commande
            continue;
        }

        // Vérifie si l'utilisateur a spécifié une commande pour envoyer un fichier
        if (strncmp(buffer, "inject", 6) == 0) {
            // Extraire les chemins des fichiers source et destination
            char *local_path = strtok(buffer, " ");
            local_path = strtok(NULL, " "); // Ignore "inject"
            char *dest_path = strtok(NULL, " ");

            // Ouvrir le fichier local en mode lecture binaire
            FILE *file = fopen(local_path, "rb");
            if (file == NULL) {
                printf("\033[31mUnable to open file: %s\033[0m\n", local_path);
                continue;
            }

            // Lire le contenu du fichier et l'envoyer au serveur
            fseek(file, 0, SEEK_END);
            long file_size = ftell(file);
            rewind(file);

            // Allouer une mémoire pour stocker le contenu du fichier
            char *file_buffer = (char *)malloc(file_size);
            if (file_buffer == NULL) {
                printf("\033[31mMemory allocation error\033[0m\n");
                fclose(file);
                continue;
            }

            // Lire le contenu du fichier
            fread(file_buffer, 1, file_size, file);
            fclose(file);

            // Envoyer le contenu du fichier au serveur
            send(sock, "FILE_CONTENT_START", strlen("FILE_CONTENT_START"), 0);
            // Envoyer d'abord la taille du fichier au serveur
            send(sock, &file_size, sizeof(file_size), 0);

            // Ensuite, envoyer le contenu du fichier
            send(sock, file_buffer, file_size, 0);
            free(file_buffer);

            // Envoyer le chemin de destination au serveur
            dest_path[strlen(dest_path)] = '\0'; 
            // Assurer que le chemin de destination est terminé par un caractère nul
            send(sock, dest_path, strlen(dest_path)+1, 0);

            // Envoyer un marqueur pour indiquer la fin de l'envoi du fichier
            send(sock, "EOF\0", 4, 0);

            // Attendre la confirmation du serveur
            int bytes_received = recv(sock, buffer, BUFFER_SIZE - 1, 0);
            if (bytes_received < 0) {
                printf("\033[31mError receiving data\033[0m\n");
            } else if (bytes_received == 0) {
                printf("\033[31mServer closed\033[0m\n");
                break; // Sortie de la boucle si la connexion avec le serveur est fermée
            }

            buffer[bytes_received] = '\0'; // Ajout du caractère de fin de chaîne
            printf("%s", buffer);
        } else {
            // Envoyer la commande normale au serveur
            send(sock, buffer, strlen(buffer), 0);

            // Vérifier si l'utilisateur a spécifié une commande pour fermer la connexion
            if (strcmp(buffer, "quit") == 0 || 
                strcmp(buffer, "exit") == 0 || 
                strcmp(buffer, "close") == 0
            ) {
                break; // Sortie de la boucle si l'utilisateur veut quitter
            }

            // Réception de la réponse du serveur
            int bytes_received = recv(sock, buffer, BUFFER_SIZE - 1, 0);
            if (bytes_received < 0) {
                printf("\033[31mError receiving data\033[0m\n");
                break;
            } else if (bytes_received == 0) {
                printf("\033[31mServer closed\033[0m\n");
                break; // Sortie de la boucle si la connexion avec le serveur est fermée
            }

            buffer[bytes_received] = '\0'; // Ajout du caractère de fin de chaîne
            printf("%s", buffer);
        }
    }
    printf("\033[35m☿ Close Mercure ☿\033[0m\n");

    close_socket(sock);
    
    #ifdef _WIN32 // Si le code est compilé sous Windows
        WSACleanup();
    #else
        return(0);
    #endif
}