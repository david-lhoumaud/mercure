# Client-Server Mercure

Mercure est composée de deux parties : un serveur et un client. Le serveur écoute les connexions des clients et exécute les commandes reçues, renvoyant les résultats au client. Le client se connecte au serveur et envoie des commandes au serveur, affichant les résultats reçus.

## Utilisation

### Serveur

Pour lancer le serveur, exécutez le fichier `server` en spécifiant le port sur lequel le serveur écoutera les connexions :

```
./server <port>
```

Par exemple, pour lancer le serveur sur le port 8080 :

```
./server 8080
```

### Client

Pour lancer le client, exécutez le fichier `client` en spécifiant l'adresse IP et le port du serveur auquel se connecter :

```
./client <server_ip> <server_port>
```

Par exemple, pour se connecter au serveur sur l'adresse IP `127.0.0.1` et le port `8080` :

```
./client 127.0.0.1 8080
```

## Fonctionnement

Une fois le serveur et le client lancés, le client affichera le symbole ☿ pour indiquer qu'il est prêt à recevoir des commandes. Le client envoie ensuite les commandes au serveur en saisissant du texte et en appuyant sur Entrée. Le serveur exécute ces commandes dans son environnement, capture les résultats et les renvoie au client. Le client affiche ensuite les résultats reçus.

Pour arrêter le client ou le serveur, vous pouvez saisir la commande `quit` ou `exit` dans le client, ou utiliser Ctrl+C dans le terminal où le programme est exécuté.