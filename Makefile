# Nom des exécutables
CLIENT_EXEC = client
SERVER_EXEC = server

CLIENT_WINDOWS_EXEC = client.exe
SERVER_WINDOWS_EXEC = server.exe

# Noms des fichiers source
CLIENT_SRC = client.c
SERVER_SRC = server.c

# Compilateur et options de compilation pour Linux
CC = gcc
CFLAGS = -Wall -Wextra -pedantic -std=c11 -static
LDFLAGS = 
#-lssl -lcrypto 

# Compilateur et options de compilation pour Windows
CC_WINDOWS = x86_64-w64-mingw32-gcc
CFLAGS_WINDOWS = -Wall -Wextra -pedantic -std=c11 -static
LDFLAGS_WINDOWS = -lws2_32 -lpthread
#-lssl -lcrypto 

# Règles par défaut pour compiler tous les exécutables
all: $(CLIENT_EXEC) $(SERVER_EXEC)

# Règle pour compiler le client sur Linux
$(CLIENT_EXEC): $(CLIENT_SRC)
	$(CC) $(CFLAGS) -o $(CLIENT_EXEC) $(CLIENT_SRC) $(LDFLAGS)

# Règle pour compiler le serveur sur Linux
$(SERVER_EXEC): $(SERVER_SRC)
	$(CC) $(CFLAGS) -o $(SERVER_EXEC) $(SERVER_SRC) $(LDFLAGS)

# Règles pour compiler tous les exécutables sur Windows
windows: $(CLIENT_WINDOWS_EXEC) $(SERVER_WINDOWS_EXEC)

# Règle pour compiler le client sur Windows
$(CLIENT_WINDOWS_EXEC): $(CLIENT_SRC)
	$(CC_WINDOWS) $(CFLAGS_WINDOWS) -o $(CLIENT_WINDOWS_EXEC) $(CLIENT_SRC) $(LDFLAGS_WINDOWS)

# Règle pour compiler le serveur sur Windows
$(SERVER_WINDOWS_EXEC): $(SERVER_SRC)
	$(CC_WINDOWS) $(CFLAGS_WINDOWS) -o $(SERVER_WINDOWS_EXEC) $(SERVER_SRC) $(LDFLAGS_WINDOWS)

# Règle pour nettoyer les fichiers générés sur Linux
clean:
	rm -f $(CLIENT_EXEC) $(SERVER_EXEC)

# Règle pour nettoyer les fichiers générés sur Windows
clean-windows:
	rm -f $(CLIENT_WINDOWS_EXEC) $(SERVER_WINDOWS_EXEC)

# Règle pour tout reconstruire après nettoyage sur Linux
rebuild: clean all

# Règle pour tout reconstruire après nettoyage sur Windows
rebuild-windows: clean-windows windows

# Indique que ces noms ne sont pas des fichiers
.PHONY: all clean rebuild windows clean-windows rebuild-windows
